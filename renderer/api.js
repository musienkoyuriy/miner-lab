(function() {
  'use strict'
  const {
    negate,
    add,
    multiply,
    divide,
    always
  } = require('ramda')

  const m = multiply

  const p1 = humidity => divide(m(divide(14.5 - humidity, 85.5), 100), 2)

  const p2 = cornMixin => m(cornMixin - 1, 0.35)

  const p3 = cornMixin2 => m(cornMixin2 - 1, 0.18)

  const p4 = weedMixin => weedMixin - 1

  const p5 = vitrescence => {
      return (vitrescence < 50) ? m(50 - vitrescence, 0.05) : 0
  }

  const p6 = nature => {
    return (nature < 775) ? m(775 - nature, 0.05) : 0
  }

  const x1 = p1 => m(p1, 0.773)

  const y1 = p1 => m(p1, 0.227)

  const z1b = x1 => m(x1, 0.667)

  const z11 = x1 => m(x1, 0.334)

  const x2 = p2 => m(negate(p2), 0.773)

  const y2 = p2 => m(negate(p2), 0.227)

  const z2b = x2 => m(x2, 0.667)

  const z21 = x2 => m(x2, 0.334)

  const x3 = p3 => negate(p3)

  const y3 = p3 => always(p3)

  const z3b = x3 => m(x3, 0.667)

  const z31 = x3 => m(x3, 0.334)

  const x4 = p4 => m(negate(p4), 0.773)

  const y4 = p4 => m(negate(p4), 0.227)

  const z4b = x4 => m(x4, 0.667)

  const z41 = x4 => m(x4, 0.334)

  const x5 = p5 => negate(p5)

  const y5 = p5 => always(p5)

  const z5b = x5 => m(x5, 0.667)

  const z51 = x5 => m(x5, 0.334)

  const x6 = p6 => negate(p6)

  const y6 = p6 => always(p6)

  const z6b = x6 => m(x6, 0.667)

  const z61 = x6 => m(x6, 0.334)

  const x7 = p2 => always(p2)

  const x8 = p1 => negate(p1)

  const x9 = p4 => always(p4)

  module.exports = {
    p1, p2, p3, p4, p5, p6,
    x1, x2, x3, x4, x5, x6, x7, x8, x9,
    z1b, z11, y2, z2b, z21, y3, z3b, z31, z4b, z41,
    y1, y5, z5b, z51, y6, z6b, z61
  }
})()
