(function() {
  'use strict'
  const db = require('../db')
  const Mustache = require('mustache')
  const $ = (sel) => document.querySelector(sel)
  const { forEach } = require('ramda')

  const cellWithValue = (value) => {
    const cell = document.createElement('td')
    cell.textContent = value
    return cell
  }

  const buildHistoryTable = (data) => {
    const historyWrapper = $('#history')
    
    forEach((item) => {
      const historyRow = document.createElement('tr')
      historyRow.appendChild(cellWithValue(item.provider))
      historyRow.appendChild(cellWithValue(item.weight))
      historyRow.appendChild(cellWithValue(item.humidity))
      historyRow.appendChild(cellWithValue(item.cornMixin))
      historyRow.appendChild(cellWithValue(item.cornMixin2))
      historyRow.appendChild(cellWithValue(item.vitrescence))
      historyRow.appendChild(cellWithValue(item.vitrescence))
      historyWrapper.appendChild(historyRow)
    }, data)
  }
  db.getData(buildHistoryTable.bind(this))
})()
