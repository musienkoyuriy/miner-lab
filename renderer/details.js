(function() {
  'use strict'
  const { ipcRenderer } = require('electron')
  const Mustache = require('mustache')
  const { add, multiply } = require('ramda')
  const Api = require('./api.js')
  const db = require('../db')

  const $ = (sel) => document.querySelectorAll(sel)
  const tableTpl = $('#corn-quality-table-tpl').innerHTML
  const buildTable = (view) => {
    const rendered = Mustache.render(tableTpl, view)
    $('#corn-quality-table').innerHTML = rendered
  }

  const BASE_HIGHER_SORT = 50
  const BASE_FIRST_SORT = 25
  const BASE_M_SORT = 75
  const BASE_BRAN_SORT = 22
  const BASE_WASTE_SORT = 2.2
  const BASE_HIGHE_SORT = 0.8

  const valuesCalculate = (data) => {
    const { humidity, cornMixin, cornMixin2, weedMixin, vitrescence, nature, weight, provider } = data
    const p1  = Api.p1(humidity).toFixed(2)
    const p2  = Api.p2(cornMixin).toFixed(2)
    const p3  = Api.p3(cornMixin2).toFixed(2)
    const p4  = Api.p4(weedMixin).toFixed(2)
    const p5  = Api.p5(vitrescence).toFixed(2)
    const p6  = Api.p6(nature).toFixed(2)
    const x1  = Api.x1(p1).toFixed(2)
    const y1  = Api.y1(p1).toFixed(2)
    const z1b = Api.z1b(x1).toFixed(2)
    const z11 = Api.z11(x1).toFixed(2)
    const x8  = Api.x8(p1).toFixed(2)
    const x2  = Api.x2(p2).toFixed(2)
    const y2  = Api.y2(p2).toFixed(2)
    const z2b = Api.z2b(x2).toFixed(2)
    const z21 = Api.z21(x2).toFixed(2)
    const x3  = Api.x3(p3).toFixed(2)
    const y3  = Api.y3(p3)
    const z3b = Api.z3b(x3)
    const z31 = Api.z31(x3).toFixed(2)
    const x4  = Api.x4(p4)
    const y4  = Api.p4(p4).toFixed(2)
    const z4b = Api.z4b(x4)
    const z41 = Api.z41(x4)
    const x5  = Api.x5(p5).toFixed(2)
    const y5  = Api.y5(p5)
    const z5b = Api.z5b(x5).toFixed(2)
    const z51 = Api.z51(x5).toFixed(2)
    const x6  = Api.x6(p6).toFixed(2)
    const y6  = Api.y6(p6)
    const z6b = Api.z6b(x6)
    const z61 = Api.z61(x6)
    const x7  = Api.x7(p2)
    const x9  = Api.x9(p4)

    const sum1 = add(z1b, z2b, z3b, z4b, z5b, z6b)
    const sum2 = add(z11, z21, z31, z41, z51, z61)
    const sum3 = add(x1, x2, x3, x4, x5, x6)
    const sum4 = add(y1, y2, y3, y4, y5, y6)
    const sum5 = add(x7, x9)

    const higherSort = add(sum1, BASE_HIGHER_SORT)
    const firstSort = add(sum2, BASE_FIRST_SORT)
    const mSort = add(sum3, BASE_M_SORT)
    const branSort = multiply(sum1, BASE_BRAN_SORT)
    const wasteSort = multiply(sum1, BASE_WASTE_SORT)

    const view = {
      humidity, cornMixin, cornMixin2, weedMixin, vitrescence, nature,
      p1, p2, p3, p4, p5, p6,
      x1, x2, x3, x4, x5, x6, x7, x8, x9,
      z1b, z11, y2, z2b, z21, y3, z3b, z31, z4b, z41,
      y5, z5b, z51, y6, z6b, z61,
      sum1, sum2, sum3, sum4, sum5
    }

    const dataForDB = {
      date: new Date(),
      humidity, cornMixin, cornMixin2, weedMixin, vitrescence, nature, weight, provider,
      higherSort, firstSort, mSort, branSort, wasteSort
    }
    db.insertData(dataForDB)
    buildTable(view)
  }

  ipcRenderer.on('new-data', (event, data) => valuesCalculate.call(null, data))
})()
