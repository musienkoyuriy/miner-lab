(function() {
  'use strict'
  const { remote, ipcRenderer } = require('electron')
  const { forEach, filter } = require('ramda')
  const main = remote.require('./main')
  const $ = (sel) => document.querySelector(sel)

  const getInputData = () => {
    return {
      humidity:    $('#humidity').value,
      cornMixin:   $('#cornMixin').value,
      cornMixin2:  $('#cornMixin2').value,
      weedMixin:   $('#weedMixin').value,
      vitrescence: $('#vitrescence').value,
      nature:      $('#nature').value,
      weight:      $('#weight').value,
      provider:    $('#provider').value
    }
  }
  const initEvents = () => {
    const inputs = Array.from(document.querySelectorAll('input[type=text]'))
    const calcBtn = $('#calcBtn')
    const historyBtn = $('#historyBtn')
    calcBtn.addEventListener('click', () => {
      main.openDetailsWnd(getInputData())
    }, false)
    historyBtn.addEventListener('click', (event) => {
      event.preventDefault()
      main.openHistoryWnd()
    }, false)
    forEach((input) => {
      input.addEventListener('keyup', () => {
        const emptyValues = filter((input) => input.value.length === 0, inputs)
        toggleCalcBtn(emptyValues, calcBtn)
      }, false)
    }, inputs)
  }
  const toggleCalcBtn = (emptyValues, calcBtn) => {
    if (emptyValues.length) {
      calcBtn.classList.add('disabled')
    } else {
      calcBtn.classList.remove('disabled')
    }
  }
  initEvents()
  document.querySelectorAll('input[type=text]')[0].focus()
})()
