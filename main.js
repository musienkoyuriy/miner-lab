const {
  app,
  BrowserWindow,
  ipc
} = require('electron')
let mainWnd
const createWindow = () => {
  mainWnd = new BrowserWindow({ width: 470, height: 710 })
  mainWnd.loadURL(`file://${__dirname}/index.html`)
  mainWnd.on('closed', () => mainWnd = null)
}
app.on('ready', createWindow)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
app.on('activate', () => {
  if (mainWnd == null) {
    createWindow()
  }
})

exports.openHistoryWnd = () => {
  const historyWnd = new BrowserWindow({ width: 1300, height: 700 })
  const { webContents } = historyWnd
  webContents.loadURL(`file://${__dirname}/history.html`)
}

exports.openDetailsWnd = (data) => {
  const detailsWnd = new BrowserWindow({ width: 1300, height: 700 })
  const { webContents } = detailsWnd
  webContents.openDevTools()
  detailsWnd.loadURL(`file://${__dirname}/details.html`)
  webContents.on('did-finish-load', () => {
    webContents.send('new-data', data)
  })
}
