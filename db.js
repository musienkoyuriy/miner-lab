const DataStore = require('nedb')

const db  = new DataStore({
  filename: `${__dirname}/minerlab.db`,
  autoload: true
})

exports.getData = (cb = () => {}) => {
  db.find({}, (err, data) => {
    if (err) {
      throw new Error('Ошибка при получении данных')
    }
    cb(data)
  })
}

exports.insertData = (newData, cb = () => {}) => {
  db.insert(newData, (err, data) => {
    if (err) {
      throw new Error('Ошибка при добавлении данных')
    }
    cb(data)
  })
}
